﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class MultiplayerMovementController : Controller
    {
        float moveSpeed = 400;
        PlayerIndex playerIndex;
        float rotationSpeed = 200;
        Actor3D playerObject;
        float deltaTimeMs;

        public MultiplayerMovementController(string id, ControllerType controllerType, PlayerIndex playerIndex, Actor3D playerObject, EventDispatcher eventDispatcher) : base(id, controllerType)
        {
            this.playerIndex = playerIndex;
            this.playerObject = playerObject;
            eventDispatcher.ControllerInput += OnControllerInput;
            eventDispatcher.CursorEvent += OnCursorEvents;
            eventDispatcher.MenuChanged += OnMenuChanged;
            eventDispatcher.UIAction += OnUIAction;
        }

        private void OnUIAction(EventData eventData)
        {
            if ((eventData.EventType == EventActionType.OnBuildMenuMode || eventData.EventType == EventActionType.OnCursorMode) && (PlayerIndex)eventData.AdditionalParameters[0] == this.playerIndex)
            {
                this.playerObject.StatusType = StatusType.Off;
            }
            else if ((eventData.EventType == EventActionType.OnExitBuildMenu || eventData.EventType == EventActionType.OnExitCursorMode) && (PlayerIndex)eventData.AdditionalParameters[0] == this.playerIndex)
            {
                this.playerObject.StatusType = StatusType.Update;
            }
        }

        private void OnMenuChanged(EventData eventData)
        {
            if (eventData.EventType == EventActionType.OnStart)
                this.playerObject.StatusType = StatusType.Update;
            else if (eventData.EventType == EventActionType.OnPause)
                this.playerObject.StatusType = StatusType.Off;
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            this.deltaTimeMs = gameTime.ElapsedGameTime.Milliseconds / 1000f;
        }

        public void OnControllerInput(EventData eventData)
        {
            // index 0 stores playerIndex
            if ((PlayerIndex)eventData.AdditionalParameters[0] == this.playerIndex && this.playerObject.GetStatusType() == StatusType.Update)
            {
                // check movement
                if (eventData.EventType == EventActionType.OnControllerLeftAnalogStick)
                {
                    Vector2 analogInput = (Vector2)eventData.AdditionalParameters[1];
                    playerObject.Transform.Translation += playerObject.Transform.Look * (analogInput.Y * deltaTimeMs * moveSpeed) + playerObject.Transform.Right * (analogInput.X * deltaTimeMs * moveSpeed);
                }
                // check rotation
                else if (eventData.EventType == EventActionType.OnControllerRightAnalogStick)
                {
                    Vector2 analogInput = (Vector2)eventData.AdditionalParameters[1];
                    playerObject.Transform.RotateAroundYBy(-analogInput.X * deltaTimeMs * rotationSpeed);
                }
            } 
        }

        public void OnCursorEvents(EventData eventData)
        {
            if ((PlayerIndex)eventData.AdditionalParameters[0] == this.playerIndex)
            {
                if (eventData.EventType == EventActionType.OnCursorPanning)
                {
                    Vector2 analogInput = (Vector2)eventData.AdditionalParameters[1];
                    playerObject.Transform.Translation += playerObject.Transform.Look * (analogInput.Y * deltaTimeMs * moveSpeed) + playerObject.Transform.Right * (analogInput.X * deltaTimeMs * moveSpeed);
                }
                else if (eventData.EventType == EventActionType.OnCursorModeRotation)
                {
                    Vector2 analogInput = (Vector2)eventData.AdditionalParameters[1];
                    playerObject.Transform.RotateAroundYBy(-analogInput.X * deltaTimeMs * rotationSpeed);
                }
            }
        }
    }
}

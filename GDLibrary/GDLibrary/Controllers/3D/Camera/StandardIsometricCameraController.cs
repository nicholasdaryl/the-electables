﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class StandardIsometricCameraController : IController
    {
        private string id;
        private PlayerIndex playerIndex;
        private ControllerType controllerType;
        private PlayStatusType playStatusType;
        private EventDispatcher eventDispatcher;
        private Actor3D target;
        private float distance;
        private float elevationAngle;
        private float zoomSpeed = 400;
        private int maxDistance;
        private int minDistance = 100;

        private float deltaTimeMs;


        public StandardIsometricCameraController(string id, ControllerType controllerType, PlayStatusType playStatusType, PlayerIndex playerIndex, Actor3D target, float distance, float elevationAngle, EventDispatcher eventDispatcher)
        {
            this.id = id;
            this.controllerType = controllerType;
            this.playStatusType = playStatusType;
            this.playerIndex = playerIndex;
            this.target = target;
            this.distance = distance;
            this.maxDistance = (int)distance;
            this.elevationAngle = elevationAngle;
            this.eventDispatcher = eventDispatcher;
    
            eventDispatcher.ControllerInput += OnControllerInput;
            eventDispatcher.MenuChanged += OnMenuChanged;
        }

        private void OnMenuChanged(EventData eventData)
        {
            if (eventData.EventType == EventActionType.OnStart)
                this.SetPlayStatus(PlayStatusType.Play);
            else if (eventData.EventType == EventActionType.OnPause)
                this.SetPlayStatus(PlayStatusType.Pause);
        }

        private void OnControllerInput(EventData eventData)
        {
            if ((PlayerIndex)eventData.AdditionalParameters[0] == this.playerIndex && this.playStatusType == PlayStatusType.Play)
            {
                // check for zoom
                if (eventData.EventType == EventActionType.OnControllerRightAnalogStick)
                {
                    Vector2 analogInput = (Vector2)eventData.AdditionalParameters[1];
                    float delta = -analogInput.Y * zoomSpeed * deltaTimeMs;
                    if (distance + delta > minDistance && distance + delta < maxDistance)
                    {
                        distance += delta;
                    } else
                    {
                        distance += delta;
                        if (distance < minDistance)
                        {
                            distance = minDistance;
                        } else if (distance > maxDistance)
                        {
                            distance = maxDistance;
                        }
                    }
                }
            }
        }

        

        public object Clone()
        {
            return new StandardIsometricCameraController(this.id, this.controllerType, this.playStatusType, this.playerIndex, this.target, this.distance, this.elevationAngle, this.eventDispatcher);
        }

        public ControllerType GetControllerType()
        {
            return this.controllerType;
        }

        public string GetID()
        {
            return this.id;
        }

        public PlayStatusType GetPlayStatus()
        {
            return this.playStatusType;
        }

        public void SetActor(IActor actor)
        {
            this.target = actor as Actor3D;
        }

        public void SetPlayStatus(PlayStatusType playStatusType)
        {
            this.playStatusType = playStatusType;
        }


        public void Update(GameTime gameTime, IActor actor)
        {
            if (this.playStatusType == PlayStatusType.Play)
            {
                    Camera3D camera = actor as Camera3D;

                    // get the reverse of the target look ( behind the target ), get the vector pointing at elevationAngle, multiply that by the distance
                    Vector3 targetToCameraVector = -target.Transform.Look + (target.Transform.Up * ((float)Math.Tan(MathHelper.ToRadians(elevationAngle)) * target.Transform.Look.Length()));

                    targetToCameraVector.Normalize();

                    // set the new camera transform to be the target translation + the newly calculated vector
                    camera.Transform.Translation = target.Transform.Translation + targetToCameraVector * distance;

                    // set look of the camera to be pointing at the target
                    camera.Transform.Look = -targetToCameraVector;

                deltaTimeMs = gameTime.ElapsedGameTime.Milliseconds / 1000f;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public class IncrementalPositionChangeController : Controller
    {
        private float speed;
        private Vector2 calculatedTranslation;
        private Vector2 targetTranslation;
        private Viewport playerViewport;

        public IncrementalPositionChangeController(string id, ControllerType controllerType, float speed, Vector2 initialTranslation, Viewport playerViewport) : base(id, controllerType)
        {
            this.speed = speed;
            this.targetTranslation = new Vector2(initialTranslation.X, initialTranslation.Y);
            this.calculatedTranslation = new Vector2(this.targetTranslation.X, this.targetTranslation.Y);
            this.playerViewport = playerViewport;
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            Actor2D actor2D = actor as Actor2D;
            Vector2 currentTranslation = actor2D.Transform.Translation;

            if (currentTranslation != targetTranslation && currentTranslation != calculatedTranslation) // means that Translation has been modified externally somewhere
            {
                targetTranslation = new Vector2(currentTranslation.X, currentTranslation.Y);
            } 

            // calculate next position if not yet reached destination
            if (calculatedTranslation != targetTranslation)
            {
                Vector2 delta = (targetTranslation - calculatedTranslation);

                float distance = delta.Length();
                //                V adjust to viewport V
                float magnitude = playerViewport.Width * speed * gameTime.ElapsedGameTime.Milliseconds / 1000f;

                delta.Normalize();

                if (distance <= magnitude)
                    calculatedTranslation = new Vector2(targetTranslation.X, targetTranslation.Y);
                else
                    calculatedTranslation += delta * magnitude;

                actor2D.Transform.Translation = new Vector2(calculatedTranslation.X, calculatedTranslation.Y);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace GDLibrary
{
    public class SelectionFrameController : Controller
    {
        private PlayerIndex playerIndex;
        private bool buildMode = false;
        private float alpha = 0;
        private float oldAlpha = 0;

        public SelectionFrameController(string id, ControllerType controllerType, PlayerIndex playerIndex, EventDispatcher eventDispatcher) : base(id, controllerType)
        {
            this.playerIndex = playerIndex;
            eventDispatcher.UIAction += OnUIAction;
            eventDispatcher.MenuChanged += OnMenuChanged;
        }

        private void OnMenuChanged(EventData eventData)
        {
            if (eventData.EventType == EventActionType.OnStart)
                this.PlayStatusType = PlayStatusType.Play;
            else if (eventData.EventType == EventActionType.OnPause)
                this.PlayStatusType = PlayStatusType.Pause;
        }

        public override void Update(GameTime gameTime, IActor actor)
        {
            UITextureObject frame = actor as UITextureObject;

            if (buildMode)
            {
                // do a pulsing alpha

                // for the moment
                frame.Color = new Color(255, 255, 255);
            } else
            {
                // fade to transparent

                // for the moment
                frame.Color = new Color(0, 0, 0, 0);
            }

            base.Update(gameTime, actor);
        }

        private void OnUIAction(EventData eventData)
        {
            if (this.PlayStatusType == PlayStatusType.Play && this.playerIndex == (PlayerIndex) eventData.AdditionalParameters[0]) {
                if (eventData.EventType == EventActionType.OnBuildMenuMode)
                {
                    this.buildMode = true;
                }
                else if (eventData.EventType == EventActionType.OnExitBuildMenu)
                {
                    this.buildMode = false;
                }
            }
        }
    }
}

﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class PlayersManager : DrawableGameComponent
    {
        public int maxPlayers;
        public List<IPlayer> players;

        public PlayersManager(Game game, int maxPlayers) : base(game)
        {
            this.maxPlayers = maxPlayers;
            players = new List<IPlayer>();
        }

        public override void Update(GameTime gameTime)
        {
            foreach(IPlayer player in players)
            {
                player.Update(gameTime);
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            foreach (IPlayer player in players)
            {
                player.DrawHUD(gameTime);
            }

            base.Draw(gameTime);
        }

        public void Add(IPlayer player)
        {
            if (players.Count < maxPlayers && players.Find(player.Equals) == null) // check if any duplicates and if max exceeded players 
            {
                players.Add(player);
            } else
            {
                throw new InvalidOperationException("Cannot add player to game due to limit reached or player has already been added.");
            }
        }
    }
}

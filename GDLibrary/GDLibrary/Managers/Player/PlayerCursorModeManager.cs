﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class PlayerCursorModeManager 
    {
        private TheElectablesPlayerHUD playerHUD;
        private UITextureObject cursor;
        private Viewport playerViewport;
        private float cursorSpeed = 1.25f;
        private float panningSpeed = 0.5f;

        // defaults
        private static Buttons TOGGLE_CURSOR_MODE = Buttons.LeftStick;
        private static Buttons EXIT_CURSOR_MODE = Buttons.B;

        public PlayerCursorModeManager(TheElectablesPlayerHUD playerHUD, UITextureObject cursor, EventDispatcher eventDispatcher)
        {
            this.playerHUD = playerHUD;
            this.cursor = cursor;
            this.playerHUD.Add(this.cursor);
            this.playerViewport = ((TheElectablesPlayer)playerHUD.player).playerCamera.Viewport;
            eventDispatcher.ControllerInput += OnControllerInput;
            eventDispatcher.UIAction += OnUIAction; // check On exit build mode and on Enter
        }

        private void OnUIAction(EventData eventData)
        {
            if ((PlayerIndex)eventData.AdditionalParameters[0] == this.playerHUD.player.PlayerIndex && (this.playerHUD.player.statusType & StatusType.Update) == StatusType.Update)
            {
                if (eventData.EventType == EventActionType.OnBuildMode)
                {
                    turnOnCursorMode();
                } else if (eventData.EventType == EventActionType.OnExitBuildMode)
                {
                    turnOffCursorMode();
                }
            }
        }

        private void OnControllerInput(EventData eventData)
        {
            if ((PlayerIndex)eventData.AdditionalParameters[0] == this.playerHUD.player.PlayerIndex && (this.playerHUD.player.statusType & StatusType.Update) == StatusType.Update && !((TheElectablesPlayer)this.playerHUD.player).buildMenuMode)
            {
                processInput(eventData);
            }
        }

        private void processInput(EventData eventData)
        {
            EventActionType eventActionType = eventData.EventType;
            object[] additionalParameters = eventData.AdditionalParameters;

            if (!((TheElectablesPlayer)this.playerHUD.player).cursorMode)
            {
                // check for button release
                if (eventActionType == EventActionType.OnControllerButtonRelease)
                {
                    // initiate build menu mode
                    Buttons clickedButton = (Buttons)additionalParameters[1];
                    if (clickedButton == PlayerCursorModeManager.TOGGLE_CURSOR_MODE)
                    {
                        turnOnCursorMode();
                    }
                }

            } else if (((TheElectablesPlayer)this.playerHUD.player).cursorMode || ((TheElectablesPlayer)this.playerHUD.player).buildMode)
            {
                // check for exit button
                if (eventActionType == EventActionType.OnControllerButtonRelease)
                {
                    // initiate build menu mode
                    Buttons clickedButton = (Buttons)additionalParameters[1];
                    if (clickedButton == PlayerCursorModeManager.EXIT_CURSOR_MODE)
                    {
                        turnOffCursorMode();
                    }
                } else if (eventActionType == EventActionType.OnControllerLeftAnalogStick) {
                    // update cursor position based on left stick analogue input
                    updateCursorPosition((Vector2)additionalParameters[1]);
                } else if (eventActionType == EventActionType.OnControllerRightAnalogStick)
                {
                    EventDispatcher.Publish(new EventData(EventActionType.OnCursorModeRotation, EventCategoryType.CursorEvent, additionalParameters));
                }
            }
        }

        private void turnOnCursorMode()
        {
            ((TheElectablesPlayer)this.playerHUD.player).cursorMode = true;

            // set to visible
            this.cursor.Color = new Color(255, 255, 255, 1f);

            // publish event
            object[] additionalParams = { this.playerHUD.player.PlayerIndex };
            EventDispatcher.Publish(new EventData(EventActionType.OnCursorMode, EventCategoryType.UIAction, additionalParams));
        }

        private void turnOffCursorMode()
        {
            ((TheElectablesPlayer)this.playerHUD.player).cursorMode = false;

            // set to Invisible
            this.cursor.Color = new Color(0, 0, 0, 0);

            // reset position to center of screen
            this.cursor.Transform.Translation = new Vector2(playerViewport.Width / 2, playerViewport.Height / 2);

            // publish event
            object[] additionalParams = { this.playerHUD.player.PlayerIndex };
            EventDispatcher.Publish(new EventData(EventActionType.OnExitCursorMode, EventCategoryType.UIAction, additionalParams));
        }

        private void updateCursorPosition(Vector2 input)
        {
            Vector2 displacement = input * TheElectablesPlayerHUD.deltaTimeSeconds * this.cursorSpeed * playerViewport.Width;

            this.cursor.Transform.Translation += new Vector2(displacement.X, -displacement.Y);

            // limit the cursor position so that it doesnt go out of bounds
            Vector2 cameraPanning = Vector2.Zero;

            if (this.cursor.Transform.Translation.X < 0)
            {
                this.cursor.Transform.Translation = new Vector2(0, this.cursor.Transform.Translation.Y);
                cameraPanning.X = -panningSpeed;
            }
            else if (this.cursor.Transform.Translation.X > this.playerViewport.Width)
            {
                this.cursor.Transform.Translation = new Vector2(this.playerViewport.Width, this.cursor.Transform.Translation.Y);
                cameraPanning.X = panningSpeed;
            }
            if (this.cursor.Transform.Translation.Y < 0)
            {
                this.cursor.Transform.Translation = new Vector2(this.cursor.Transform.Translation.X, 0);
                cameraPanning.Y = panningSpeed;
            }
            else if (this.cursor.Transform.Translation.Y > this.playerViewport.Height)
            {
                this.cursor.Transform.Translation = new Vector2(this.cursor.Transform.Translation.X, this.playerViewport.Height);
                cameraPanning.Y = -panningSpeed;
            }

            if (cameraPanning != Vector2.Zero) {
                object[] additionalParams = { this.playerHUD.player.PlayerIndex, cameraPanning };
                EventDispatcher.Publish(new EventData(EventActionType.OnCursorPanning, EventCategoryType.CursorEvent, additionalParams));
            }
        }
    }
}

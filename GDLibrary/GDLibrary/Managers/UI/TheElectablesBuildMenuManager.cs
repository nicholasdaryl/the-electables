﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class TheElectablesBuildMenuManager
    {
        #region Variables
        // handle to player
        public TheElectablesPlayerHUD playerHUD;

        // stores information for menu processsing
        public Dictionary<string, List<BuildMenuOption>> sections = new Dictionary<string, List<BuildMenuOption>>();
        public List<BuildMenuOption> activeSection = new List<BuildMenuOption>();
        public BuildMenuOption activeSelection = null;
        public int activeSelectionIndex = 0;
        private int numberOfOptions;
        private Stack<string> sectionNameHistory = new Stack<string>();
        

        // UI Overlay to indicate selection
        public UITextureObject selectionFrame = null;
        
        // defaults
        public static string DEFAULT_SECTION_NAME = "categories";
        public static Buttons TOGGLE_BUILD_MENU = Buttons.Y;
        public static Buttons NEXT_PAGE_LEFT = Buttons.LeftShoulder;
        public static Buttons NEXT_PAGE_RIGHT = Buttons.RightShoulder;
        public static Buttons SELECT_BUTTON = Buttons.A | Buttons.DPadUp;
        public static Buttons CANCEL_BUTTON = Buttons.B | Buttons.DPadDown;
        public static Buttons NAVIGATE_LEFT = Buttons.DPadLeft;
        public static Buttons NAVIGATE_RIGHT = Buttons.DPadRight;

        // for initializing positions of elements
        float originalBarWidth, barWidth, barXPosition, barYPosition;
        Vector2 scale;
        private float buildBarEdgeX;
        #endregion

        #region Constructors
        public TheElectablesBuildMenuManager(TheElectablesPlayerHUD playerHUD, EventDispatcher eventDispatcher, int numberOfOptions, float originalBarWidth, float barWidth, float barXPosition, float barYPosition, Vector2 scale)
        {
            this.scale = scale;
            this.originalBarWidth = originalBarWidth;
            this.barWidth = barWidth;
            this.barXPosition = barXPosition;
            this.barYPosition = barYPosition;
            this.numberOfOptions = numberOfOptions;
            this.playerHUD = playerHUD;
            this.sectionNameHistory.Push(TheElectablesBuildMenuManager.DEFAULT_SECTION_NAME);
            eventDispatcher.ControllerInput += handleControllerInput;
        }
        #endregion 

        #region Initialization Methods
        public void AddToSection(string sectionName, Texture2D texture, string forwardsSectionName)
        {

            Vector2 origin = new Vector2(texture.Width / 2f, texture.Height / 2f);
            Integer2 dimensions = new Integer2(texture.Width, texture.Height);
            Transform2D transform = new Transform2D(Vector2.Zero, 0, scale, origin, dimensions);

            UITextureObject uiTextureObject = new UITextureObject("build_option", ActorType.UITexture, StatusType.Update | StatusType.Drawn, transform, Color.White, SpriteEffects.None, 0.95f, texture);

            try
            {
                this.sections[sectionName].Add(new BuildMenuOption(uiTextureObject, forwardsSectionName));
            } catch (KeyNotFoundException e) // if key not found
            {
                this.sections.Add(sectionName, new List<BuildMenuOption>());

                this.sections[sectionName].Add(new BuildMenuOption(uiTextureObject, forwardsSectionName));
            }
        }

        public void setupPositions(float viewportWidth, float Ymultiplier)
        {
            // initialize icon positions using for loop

            this.buildBarEdgeX = this.barXPosition - ((originalBarWidth * scale.X) / 2f);

            foreach (KeyValuePair<string, List<BuildMenuOption>> section in sections)
            {
                for (int i = 0; i < numberOfOptions; i++)
                {
                    if (i < section.Value.Count)
                    {
                        section.Value[i].textureObject.Transform.Translation = new Vector2(buildBarEdgeX + ((((i * 2) % (numberOfOptions * 2)) + 1) * (this.barWidth / (numberOfOptions * 2f))), this.barYPosition - (viewportWidth * Ymultiplier));
                    }
                    else
                    {
                        break;
                    }
                }
            }
        }
        internal void SetSelectionFrame(UITextureObject selectionFrame, EventDispatcher eventDispatcher)
        {
            this.selectionFrame = selectionFrame;
            this.selectionFrame.AttachController(new SelectionFrameController("Selection Frame Controller", ControllerType.SelectionFrame, this.playerHUD.player.PlayerIndex, eventDispatcher));
            this.updateSelectionFramePosition();
            this.selectionFrame.AttachController(new IncrementalPositionChangeController("Incremental Position Controller", ControllerType.Movement, 3f, this.selectionFrame.Transform.Translation, ((TheElectablesPlayer)this.playerHUD.player).playerCamera.Viewport));
        }

        public void reset()
        {
            this.activeSelectionIndex = 0;
            this.updateActiveSelection();
            this.updateSelectionFramePosition();
        }
        #endregion

        #region Update Menu Methods
        public void setActiveSection(string sectionName)
        {
            // remove from draw list
            foreach (BuildMenuOption menuOption in this.activeSection)
            {
                this.playerHUD.Remove(menuOption.textureObject);
            }

            // set new active section
            this.activeSection = sections[sectionName];

            // add to draw list
            foreach (BuildMenuOption menuOption in this.activeSection)
            {
                this.playerHUD.Add(menuOption.textureObject);
            }
        }

        public void updateActiveSelection()
        {
            try
            {
                this.activeSelection = this.activeSection[activeSelectionIndex];
            }
            catch (ArgumentOutOfRangeException e)
            {
                this.activeSelection = null;
            }

        }

        public void updateSelectionFramePosition()
        {
            int position = this.activeSelectionIndex % numberOfOptions;

            this.selectionFrame.Transform.Translation = new Vector2(buildBarEdgeX + (barWidth / (numberOfOptions * 2f)) + ((barWidth / numberOfOptions) * position), this.selectionFrame.Transform.Translation.Y);
        }
        #endregion

        #region Menu Input Processing

        private void processInput(EventData eventData)
        {
            EventActionType eventActionType = eventData.EventType;
            object[] additionalParameters = eventData.AdditionalParameters;

            // check whether or not in build menu mode
            if (!((TheElectablesPlayer)this.playerHUD.player).buildMenuMode) {
                // check for button clicks 
                if (eventActionType == EventActionType.OnControllerButtonRelease)
                {
                    // initiate build menu mode
                    Buttons clickedButton = (Buttons)additionalParameters[1];
                    if (clickedButton == Buttons.Y)
                    {
                        initiateBuildMenu();
                    }
                }
            } else // if is currently in build mode
            {
                // check for button clicks 
                if (eventActionType == EventActionType.OnControllerButtonRelease)
                {
                    // initiate build menu mode
                    Buttons clickedButton = (Buttons)additionalParameters[1];
                    if (clickedButton == Buttons.A)
                    {
                        GoForward();
                    } else if (clickedButton == Buttons.B)
                    {
                        GoBack();
                    } else if (clickedButton == Buttons.DPadLeft || clickedButton == Buttons.LeftThumbstickLeft)
                    {
                        GoLeft();
                    } else if (clickedButton == Buttons.DPadRight || clickedButton == Buttons.LeftThumbstickRight)
                    {
                        GoRight();
                    }
                } 

                // finally always update the activeSelection
                this.updateActiveSelection();
                this.updateSelectionFramePosition();
            }
            
        }

        private void exitBuildMenu()
        {
            this.sectionNameHistory.Push(TheElectablesBuildMenuManager.DEFAULT_SECTION_NAME);
            ((TheElectablesPlayer)this.playerHUD.player).buildMenuMode = false;
            // publish event to exit build menu
            object[] additionalParams = { this.playerHUD.player.PlayerIndex };
            EventDispatcher.Publish(new EventData(EventActionType.OnExitBuildMenu, EventCategoryType.UIAction, additionalParams));
        }

        private void initiateBuildMenu()
        {
            ((TheElectablesPlayer)this.playerHUD.player).buildMenuMode = true;
            object[] additionalParams = { this.playerHUD.player.PlayerIndex };
            EventDispatcher.Publish(new EventData(EventActionType.OnBuildMenuMode, EventCategoryType.UIAction, additionalParams));
        }

        #region Menu Navigation Methods
        private void GoForward()
        {
            if (this.activeSelection != null && this.sections.ContainsKey(this.activeSelection.forwardsSectionName))
            {
                this.sectionNameHistory.Push(this.activeSelection.forwardsSectionName);
                setActiveSection(this.activeSelection.forwardsSectionName);
                this.activeSelectionIndex = 0;
            } else if(this.activeSelection != null && this.activeSelection.forwardsSectionName.Contains("build"))
            {
                // publish event to build passing in the command name which will later be handled in the BuildModeManager
                object[] additionalParams = { this.playerHUD.player.PlayerIndex, this.activeSelection.forwardsSectionName };
                // EventDispatcher.Publish( new EventData(EventActionType.OnStartBuildMode, EventCategoryType.BuildMode, additionalParams));
            }
        }

        private void GoBack()
        {
            string forwardsSectionName = this.sectionNameHistory.Pop();
            // check if current category name == default name, if yes then exit Build Menu
            if (forwardsSectionName == TheElectablesBuildMenuManager.DEFAULT_SECTION_NAME)
            {
                exitBuildMenu();
                return;
            }

            // else process the navigation
            
            setActiveSection(this.sectionNameHistory.Last());
            this.activeSelectionIndex = this.activeSection.FindIndex((menuOption) => (menuOption.forwardsSectionName == forwardsSectionName));
        }

        private void GoRight()
        {
            int modulus = this.activeSelectionIndex % numberOfOptions;

            // makes sure it doesn't go out of bounds
            if (modulus == numberOfOptions - 1)
            {
                return;
            }

            // if not out of bounds then advance to the right option
            this.activeSelectionIndex++;
        }

        private void GoLeft()
        {
            int modulus = this.activeSelectionIndex % numberOfOptions;

            // makes sure it doesn't go out of bounds
            if (modulus == 0)
            {
                return;
            }

            // if not out of bounds then advance to the left option
            this.activeSelectionIndex--;
        }
        #endregion

        #endregion

        #region Event Handling
        public void handleControllerInput(EventData eventData)
        {
            // player index check, player status check, buildModeStatus check
            if ((PlayerIndex)eventData.AdditionalParameters[0] == this.playerHUD.player.PlayerIndex && (this.playerHUD.player.statusType & StatusType.Update) == StatusType.Update && !((TheElectablesPlayer)this.playerHUD.player).buildMode)
            {
                processInput(eventData);
            }
        }
        #endregion 
    }
}

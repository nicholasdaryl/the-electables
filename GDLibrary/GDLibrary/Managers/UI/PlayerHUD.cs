﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public abstract class PlayerHUD // this class is meant to be derived from and is meant to only be composed in the player class
    {
        // handle to player
        public Player player;
        // handle to spritebatch
        protected SpriteBatch spriteBatch;
        //private StatusType statusType; // doesnt need a status type as already being handled in the player class

        protected List<UIObject> hudElements;

        public PlayerHUD(Player player, SpriteBatch spriteBatch)
        {
            this.player = player;
            this.spriteBatch = spriteBatch;

            hudElements = new List<UIObject>();
        }

        public virtual void UpdateAndDraw(GameTime gameTime)
        {
            // no status type check needed

            // update element controllers
            foreach(UIObject hudElement in hudElements)
            {
                if ((hudElement.StatusType & StatusType.Update) == StatusType.Update)
                {
                    hudElement.Update(gameTime);
                }
            }

            // draw each element

            // set the graphics device viewport to be the current player's camera viewport


            this.spriteBatch.Begin(SpriteSortMode.BackToFront, BlendState.AlphaBlend);
            foreach (UIObject hudElement in hudElements)
            {
                if ((hudElement.StatusType & StatusType.Drawn) == StatusType.Drawn)
                {
                    hudElement.Draw(gameTime, this.spriteBatch);
                }
            }
            this.spriteBatch.End();
        }

        public void Add(UIObject element)
        {
            hudElements.Add(element);
        }

        public void RemoveAll(UIObject element)
        {
            hudElements.RemoveAll((hudElement) => (element == hudElement));
        }

        public void Remove(UIObject element)
        {
            hudElements.Remove(element);
        }
    }
}

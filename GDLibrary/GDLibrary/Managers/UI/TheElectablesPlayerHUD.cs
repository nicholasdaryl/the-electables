﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace GDLibrary
{
    public class TheElectablesPlayerHUD : PlayerHUD
    {
        // required information
        private TheElectablesBuildMenuManager buildMenuManager;
        private PlayerCursorModeManager cursorModeManager;

        // statics
        private static ContentDictionary<Texture2D> textureDictionary;
        private static int howManyBuildPanels = 5;
        public static float deltaTimeSeconds = 0f;

        // size information
        float buildBarWidth;
        float buildBarXPosition;
        float buildBarYPosition;

        // variables that need to be edited


        public TheElectablesPlayerHUD(TheElectablesPlayer player, SpriteBatch spriteBatch, EventDispatcher eventDispatcher, ContentDictionary<Texture2D> textureDictionary) : base(player, spriteBatch)
        {
            // setup handles
            TheElectablesPlayerHUD.textureDictionary = textureDictionary;

            // setup event Handling, if any 

            /* Add UI objects to elements list here */

            // set up the boundary for drawing objects. Here the boundary is determined by this player's camera viewport
            Viewport playerCameraViewport = player.playerCamera.Viewport;
            Transform2D transform;
            Vector2 translation;
            Vector2 scale;
            Vector2 origin;
            Integer2 dimensions;
            float rotation = 0;
            float scalar = 0.001041666666666f;
            float iconPositionOffsetMultiplier = 0.0075f;
            float scaleWithRespectToViewportWidth = playerCameraViewport.Width * scalar;
            Texture2D texture;

            #region Build Menu HUD

            #region Bottom Selection Panels
            texture = textureDictionary["selectionPanels"];

            this.buildBarWidth = texture.Width * scaleWithRespectToViewportWidth;
            this.buildBarYPosition = playerCameraViewport.Height * (9f / 10f);
            this.buildBarXPosition = (playerCameraViewport.Width / 2f);

            translation = new Vector2(this.buildBarXPosition, this.buildBarYPosition);
            scale = new Vector2(scaleWithRespectToViewportWidth, scaleWithRespectToViewportWidth);
            origin = new Vector2(texture.Width / 2f, texture.Height / 2f);
            dimensions = new Integer2(texture.Width, texture.Height);
            transform = new Transform2D(translation, rotation, scale, origin, dimensions);
            this.Add(new UITextureObject("selectionPanels", ActorType.UITexture, StatusType.Update | StatusType.Drawn, transform, Color.White, SpriteEffects.None, 0.96f, texture));
            #endregion

            #region Selection Frame
            texture = textureDictionary["selectionFrame"];
            origin = new Vector2(texture.Width / 2f, texture.Height / 2f);
            dimensions = new Integer2(texture.Width, texture.Height);
            transform = new Transform2D(translation, rotation, scale, origin, dimensions);
            UITextureObject selectionFrame = new UITextureObject("selectionFrame", ActorType.UITexture, StatusType.Update | StatusType.Drawn, transform, Color.White, SpriteEffects.None, 0.9f, texture);
            this.Add(selectionFrame);
            #endregion

            #region Build Menu

            this.buildMenuManager = new TheElectablesBuildMenuManager(this, eventDispatcher, TheElectablesPlayerHUD.howManyBuildPanels, textureDictionary["selectionPanels"].Width, buildBarWidth, buildBarXPosition, buildBarYPosition, scale);

            // add build icons to Build Manager
            #region categories
            this.buildMenuManager.AddToSection("categories", textureDictionary["houseIcon"], "housing");
            this.buildMenuManager.AddToSection("categories", textureDictionary["resourcesIcon"], "resources");
            this.buildMenuManager.AddToSection("categories", textureDictionary["workplaceIcon"], "workplaces");
            this.buildMenuManager.AddToSection("categories", textureDictionary["emergencyIcon"], "emergencyServices");
            this.buildMenuManager.AddToSection("categories", textureDictionary["transportationIcon"], "transportationServices");
            #endregion

            #region transportationServices
            this.buildMenuManager.AddToSection("transportationServices", textureDictionary["transportationIcon"], "trainStation");
            #endregion 

            // finalize the setup
            this.buildMenuManager.setupPositions(playerCameraViewport.Width, iconPositionOffsetMultiplier);
            this.buildMenuManager.setActiveSection(TheElectablesBuildMenuManager.DEFAULT_SECTION_NAME);

            // set the selectionFrame
            this.buildMenuManager.SetSelectionFrame(selectionFrame, eventDispatcher);

            this.buildMenuManager.reset();

            #endregion

            #endregion

            #region Cursor
            translation = new Vector2(playerCameraViewport.Width/2f, playerCameraViewport.Height/2f);
            origin = new Vector2(0f, 0f);
            dimensions = new Integer2(texture.Width, texture.Height);
            transform = new Transform2D(translation, rotation, scale, origin, dimensions);
            this.cursorModeManager = new PlayerCursorModeManager(this, new UITextureObject("playerCursor", ActorType.UIMouse, StatusType.Drawn | StatusType.Update, transform, new Color(0, 0, 0, 0), SpriteEffects.None, 0.97f, textureDictionary["cursor"]), eventDispatcher);
            #endregion
        }

        public override void UpdateAndDraw(GameTime gameTime)
        {
            TheElectablesPlayerHUD.deltaTimeSeconds = gameTime.ElapsedGameTime.Milliseconds / 1000f;
            // update any text or scores here first



            // set the graphics device viewport to be the current player's viewport before drawing!!
            this.spriteBatch.GraphicsDevice.Viewport = ((TheElectablesPlayer)this.player).playerCamera.Viewport;

            base.UpdateAndDraw(gameTime);
        }
    }
}

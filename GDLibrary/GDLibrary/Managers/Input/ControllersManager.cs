﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace GDLibrary
{
    public class ControllersManager : GameComponent
    {
        static int maxNumberOfPlayers = 4;
        GamePadState[] currentStates = new GamePadState[maxNumberOfPlayers];
        GamePadState[] oldStates = new GamePadState[maxNumberOfPlayers];
        Buttons[] buttonsToBeChecked = {
            Buttons.A,
            Buttons.B,
            Buttons.X,
            Buttons.Y,
            Buttons.LeftShoulder,
            Buttons.RightShoulder,
            Buttons.LeftStick,
            Buttons.LeftThumbstickLeft,
            Buttons.LeftThumbstickRight,
            Buttons.DPadLeft,
            Buttons.DPadRight,
            Buttons.DPadUp,
            Buttons.DPadDown,
        };
        public ControllersManager(Game game) : base(game)
        {

        }
        public override void Update(GameTime gameTime)
        {
            // get the states for all players
            getStates();

            // check connection status
            this.checkConnections();

            checkInput();

            // set old states to current states
            oldStates = currentStates.Clone() as GamePadState[];

            base.Update(gameTime);
        }

        private void checkInput()
        {
            for (int i = 0; i < maxNumberOfPlayers; i++)
            {
                #region Check Buttons

                foreach (Buttons button in buttonsToBeChecked)
                {
                    // check for press
                    if (currentStates[i].IsButtonDown(button))
                    {
                        object[] additionalParamaters = { (PlayerIndex)i, button };
                        if (oldStates[i].IsButtonUp(button)) // checks if first time
                        {
                            //publish Event button pressed for the first time
                            EventDispatcher.Publish(new EventData(EventActionType.OnControllerButtonPressed, EventCategoryType.ControllerInput, additionalParamaters));
#if DEBUG
                            System.Diagnostics.Debug.WriteLine("Pressed " + button.ToString());
#endif
                        }
                        //publish Event button is being held down
                        EventDispatcher.Publish(new EventData(EventActionType.OnControllerButtonHold, EventCategoryType.ControllerInput, additionalParamaters));
#if DEBUG
                        System.Diagnostics.Debug.WriteLine("Holding down " + button.ToString());
#endif
                    }

                    if (currentStates[i].IsButtonUp(button) && oldStates[i].IsButtonDown(button))
                    {
                        object[] additionalParamaters = { (PlayerIndex)i, button };
                        //publish Event button released 
                        EventDispatcher.Publish(new EventData(EventActionType.OnControllerButtonRelease, EventCategoryType.ControllerInput, additionalParamaters));
#if DEBUG
                        System.Diagnostics.Debug.WriteLine("Released " + button.ToString());
#endif

                    }
                }

                #endregion

                #region Analog Sticks
                if (currentStates[i].ThumbSticks.Left.X != 0 || currentStates[i].ThumbSticks.Left.Y != 0)
                {
                    // publish event ControllerLeftStickMove
                    object[] additionalParams = { (PlayerIndex)i, new Vector2(currentStates[i].ThumbSticks.Left.X, currentStates[i].ThumbSticks.Left.Y) };
                    EventDispatcher.Publish(new EventData(EventActionType.OnControllerLeftAnalogStick, EventCategoryType.ControllerInput, additionalParams));
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("Left Stick: " + currentStates[i].ThumbSticks.Left);
#endif
                }
                if (currentStates[i].ThumbSticks.Right.X != 0 || currentStates[i].ThumbSticks.Right.Y != 0)
                {
                    // publish event ControllerLeftStickMove
                    object[] additionalParams = { (PlayerIndex)i, new Vector2(currentStates[i].ThumbSticks.Right.X, currentStates[i].ThumbSticks.Right.Y) };
                    EventDispatcher.Publish(new EventData(EventActionType.OnControllerRightAnalogStick, EventCategoryType.ControllerInput, additionalParams));
#if DEBUG
                    System.Diagnostics.Debug.WriteLine("Right Stick: " + currentStates[i].ThumbSticks.Right);
#endif
                }
                #endregion
            }
        }

        private void getStates()
        {
            for (int i = 0; i < maxNumberOfPlayers; i++)
            {
                currentStates[i] = GamePad.GetState((PlayerIndex)i);
            }
        }

        private void checkConnections()
        {
            for (int i = 0; i < maxNumberOfPlayers; i++)
            {
                if (currentStates[i].IsConnected) // connected
                {
                    if (oldStates[i].IsConnected == false) // check if previously disconnected
                    {
                        // publish Event controller i has connected
#if DEBUG
                        System.Diagnostics.Debug.WriteLine("Player " + (i+1) + " connected.");
#endif
                    }
                } else // disconnected
                {
                    if (oldStates[i].IsConnected == true) // check if previously connected
                    {
                        // publish Event controller i has disconnected
#if DEBUG
                        System.Diagnostics.Debug.WriteLine("Player " + (i + 1) + " disconnected.");
#endif
                    }
                }
            }
        }
    }
}

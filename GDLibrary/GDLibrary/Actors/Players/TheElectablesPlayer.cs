﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace GDLibrary
{
    public class TheElectablesPlayer : Player
    {

        public Camera3D playerCamera;
        public ObjectManager objectManager;
        public PrimitiveObject objectBuffer;
        public TheElectablesPlayerHUD playerHUD;
        //public BuildModeManager buildModeManager;

        // booleans required to check player state
        public bool buildMode = false;
        public bool buildMenuMode = false;
        public bool cursorMode = false;

        public TheElectablesPlayer(string name, PlayerIndex playerIndex, Camera3D camera, Dictionary<string, object> additional) : base(name, playerIndex)
        {
            this.playerCamera = camera;
            objectBuffer = null;

            playerHUD = new TheElectablesPlayerHUD(this, (SpriteBatch)additional["SpriteBatch"], (EventDispatcher)additional["EventDispatcher"], (ContentDictionary<Texture2D>)additional["TextureDictionary"]);

            ((EventDispatcher)additional["EventDispatcher"]).MenuChanged += OnPauseOrStart;


            // set initial status
            this.statusType = StatusType.Update | StatusType.Drawn;
        }

        public override void Update(GameTime gameTime)
        {
            if ((this.statusType & StatusType.Update) == StatusType.Update)
            {
                // place object to be built on the ground
                if (objectBuffer != null) {
                    placeObjectInBufferAtCursor();
                }
            }
        }

        public override void DrawHUD(GameTime gameTime)
        {
            if ((this.statusType & StatusType.Drawn) == StatusType.Drawn)
            {
                playerHUD.UpdateAndDraw(gameTime);
            }
        }

        private void placeObjectInBufferAtCursor()
        {
            if (objectBuffer != null)
            {
                // place the object on the surface where the cursor is pointing at
            }
        }

        private void OnPauseOrStart(EventData eventData)
        {
            if (eventData.EventType == EventActionType.OnStart)
                this.statusType = StatusType.Update | StatusType.Drawn;
            if (eventData.EventType == EventActionType.OnPause)
                this.statusType = StatusType.Off;
            if (eventData.EventType == EventActionType.OnGameEnd)
                this.statusType = StatusType.Drawn;
        }
    }
}

﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public abstract class Player : IPlayer
    {
        #region Variables

        // Essentials
        private string name;
        private PlayerIndex playerIndex;
        public StatusType statusType;

        #endregion

        #region Getters and Setters
        public PlayerIndex PlayerIndex
        {
            get
            {
                return this.playerIndex; 
            }
            set
            {
                this.playerIndex = value;
            }
        }

        public string Name
        {
            get
            {
                return this.name;
            }
            set
            {
                this.name = value;
            }
        }
        #endregion

        #region Constructors
        public Player(string name, PlayerIndex playerIndex)
        {
            this.name = name;
            this.playerIndex = playerIndex;
        }
        #endregion 

        public virtual void Update(GameTime gameTime)
        {
            // will be overriden
        }

        public virtual void DrawHUD(GameTime gameTime)
        {
            // will be overriden
        }

        public bool Equals(IPlayer otherPlayer)
        {
            return ((Player)otherPlayer).PlayerIndex == this.PlayerIndex;        
        }

    }
}

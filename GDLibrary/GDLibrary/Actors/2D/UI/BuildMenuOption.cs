﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public class BuildMenuOption
    {
        public UITextureObject textureObject;
        public string forwardsSectionName;

        public BuildMenuOption(UITextureObject textureObject, string forwardsName) 
        {
            this.textureObject = textureObject;
            this.forwardsSectionName = forwardsName;
        }
    }
}

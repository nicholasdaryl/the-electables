﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace GDLibrary
{
    public interface IPlayer
    {
        void Update(GameTime gameTime);
        void DrawHUD(GameTime gameTime);
        bool Equals(IPlayer otherPlayer);
    }
}
